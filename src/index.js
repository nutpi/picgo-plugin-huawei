/**
 * @file PicGo 华为云 OBS 上传插件
 * @author 坚果派 <jianguo@nutpi.net>
 * @copyright Copyright (c) 2024 坚果派
 * @license MIT License
 * @version 1.0.3
 * @created 2024-02-01
 * @modified 2024-02-21
 * @description 支持将图片上传到华为云对象存储，并作为图床使用
 * @homepage https://nutpi.net
 * @repository https://gitcode.com/nutpi/picgo-plugin-huawei
 */

const mime = require('mime/lite')  // 使用更轻量的版本
const crypto = require('crypto')

/**
 * 常量配置
 * 集中管理插件中使用的所有常量值
 */
const CONSTANTS = {
  PLUGIN_NAME: 'huaweicloud-uploader',
  DISPLAY_NAME: '华为云OBS',
  CONFIG_PATH: 'picBed.huaweicloud-uploader',
  LOG_PREFIX: '[华为云OBS]',
  HTTP_METHOD: 'PUT',
  DEFAULT_CACHE_CONTROL: 'max-age=31536000' // 默认缓存控制，一年
}

/**
 * 错误类型定义
 * 定义可能遇到的华为云OBS错误类型，用于更友好的错误提示
 */
const ERROR_TYPES = {
  ACCESS_DENIED: 'Access Denied',
  NO_SUCH_BUCKET: 'NoSuchBucket',
  INVALID_ACCESS_KEY: 'InvalidAccessKeyId',
  SIGNATURE_MISMATCH: 'SignatureDoesNotMatch'
}

/**
 * 日志工具
 * 统一的日志输出接口，便于调试和问题排查
 */
const logger = {
  info: (message) => console.log(`${CONSTANTS.LOG_PREFIX} ${message}`),
  error: (message) => console.error(`${CONSTANTS.LOG_PREFIX} ${message}`),
  debug: (message, data) => console.log(`${CONSTANTS.LOG_PREFIX} ${message}`, data || '')
}

/**
 * 生成华为云 OBS 签名
 * 根据华为云OBS API要求生成认证签名
 * 
 * @param {Object} options - 上传配置项
 * @param {string} fileName - 文件名
 * @returns {string} 签名字符串
 * @throws {Error} 当无法获取 MIME 类型时抛出错误
 */
const generateSignature = (options, fileName) => {
  logger.info(`开始生成签名，文件名: ${fileName}`)

  const date = new Date().toUTCString()
  const mimeType = mime.getType(fileName)

  if (!mimeType) {
    logger.error(`无法获取文件 ${fileName} 的 MIME 类型`)
    throw Error(`无法识别文件类型: ${fileName}`)
  }

  // 构建签名字符串，格式遵循华为云OBS API规范
  const path = options.path
  const strToSign = `${CONSTANTS.HTTP_METHOD}\n\n${mimeType}\n${date}\n/${options.bucketName}${path ? '/' + encodeURI(options.path) : ''}/${encodeURI(fileName)}`

  logger.debug('生成的签名字符串:', strToSign)

  // 使用HMAC-SHA1算法和AccessKeySecret生成签名
  const signature = crypto
    .createHmac('sha1', options.accessKeySecret)
    .update(strToSign)
    .digest('base64')

  logger.info('签名生成完成')
  return `OBS ${options.accessKeyId}:${signature}`
}

/**
 * 构建上传请求选项
 * 根据华为云OBS API要求构建HTTP请求参数
 * 
 * @param {Object} options - 上传配置项
 * @param {string} fileName - 文件名
 * @param {string} signature - 签名字符串
 * @param {Buffer} image - 图片数据
 * @returns {Object} 请求配置对象
 */
const postOptions = (options, fileName, signature, image) => {
  logger.info(`构建上传请求参数，文件名: ${fileName}`)

  const path = options.path
  const mimeType = mime.getType(fileName)
  // 构建完整的上传URL
  const url = `https://${options.bucketName}.${options.endpoint}${path ? '/' + encodeURI(options.path) : ''}/${encodeURI(fileName)}`

  logger.debug('上传URL:', url)

  // 返回完整的请求配置
  return {
    method: CONSTANTS.HTTP_METHOD,
    url,
    headers: {
      Authorization: signature,
      Date: new Date().toUTCString(),
      'content-type': mimeType,
      'Cache-Control': options.cacheControl || CONSTANTS.DEFAULT_CACHE_CONTROL
    },
    body: image,
    resolveWithFullResponse: true
  }
}

/**
 * 处理错误消息
 * 将华为云OBS的错误转换为用户友好的提示信息
 * 
 * @param {Error} error - 错误对象
 * @returns {string} 格式化的错误消息
 */
const handleError = (error) => {
  const message = error.message

  // 针对不同错误类型提供更友好的错误提示
  const errorMessages = {
    [ERROR_TYPES.ACCESS_DENIED]: '访问被拒绝，请检查：\n1. AccessKey 是否正确\n2. 桶名称是否正确\n3. 地域节点是否正确\n4. AccessKey 是否有权限',
    [ERROR_TYPES.NO_SUCH_BUCKET]: '存储桶不存在，请检查桶名称是否正确',
    [ERROR_TYPES.INVALID_ACCESS_KEY]: 'AccessKey ID 无效',
    [ERROR_TYPES.SIGNATURE_MISMATCH]: 'AccessKey Secret 不正确'
  }

  // 匹配错误类型并返回对应的友好提示
  for (const [errorType, errorMessage] of Object.entries(errorMessages)) {
    if (message.includes(errorType)) {
      return errorMessage
    }
  }

  return message || '未知错误'
}

/**
 * 验证配置项
 * 确保所有必要的配置项都已提供
 * 
 * @param {Object} options - 配置项
 * @throws {Error} 当必要配置缺失时抛出错误
 */
const validateConfig = (options) => {
  if (!options) {
    throw new Error('找不到华为OBS图床配置文件')
  }

  // 定义必填字段及其显示名称
  const requiredFields = {
    accessKeyId: 'AccessKey ID',
    accessKeySecret: 'AccessKey Secret',
    bucketName: '桶名称',
    endpoint: '地域节点'
  }

  // 检查每个必填字段
  for (const [field, name] of Object.entries(requiredFields)) {
    if (!options[field]) {
      throw new Error(`请配置${name}`)
    }
  }
}

/**
 * 处理单个图片上传
 * 完成单张图片的上传流程
 * 
 * @param {Object} ctx - PicGo 上下文
 * @param {Object} obsOptions - OBS 配置项
 * @param {Object} img - 图片对象
 * @returns {Promise<void>}
 */
const handleSingleImage = async (ctx, obsOptions, img) => {
  logger.info(`开始处理图片: ${img.fileName}`)

  // 验证图片数据完整性
  if (!img.fileName || (!img.buffer && !img.base64Image)) {
    throw new Error('图片数据不完整')
  }

  // 生成上传签名
  const signature = generateSignature(obsOptions, img.fileName)
  let image = img.buffer

  // 如果没有buffer但有base64数据，则进行转换
  if (!image && img.base64Image) {
    logger.info('转换base64图片数据为Buffer')
    image = Buffer.from(img.base64Image, 'base64')
  }

  // 构建请求选项并发送请求
  const options = postOptions(obsOptions, img.fileName, signature, image)
  logger.info('开始上传图片')

  const response = await ctx.request(options)

  // 检查响应状态
  if (!response || response.statusCode !== 200) {
    throw new Error(`上传失败: HTTP ${response?.statusCode || 'unknown'}`)
  }

  // 上传成功后清理临时数据
  logger.info('图片上传成功，清理临时数据')
  delete img.base64Image
  delete img.buffer

  // 构建图片访问URL
  const path = obsOptions.path
  const domain = obsOptions.customDomain || `https://${obsOptions.bucketName}.${obsOptions.endpoint}`
  img.imgUrl = `${domain}${path ? '/' + encodeURI(path) : ''}/${encodeURI(img.fileName)}`

  logger.debug('生成访问链接:', img.imgUrl)

  // 添加图片处理参数（如果有）
  if (obsOptions.imageProcess) {
    img.imgUrl += obsOptions.imageProcess
    logger.debug('添加图片处理参数后的最终链接:', img.imgUrl)
  }
}

/**
 * 处理图片上传的主函数
 * 协调整个上传流程
 * 
 * @param {Object} ctx - PicGo 上下文
 * @returns {Promise<Object>} Promise<picgo>
 */
const handle = async (ctx) => {
  logger.info('开始处理上传任务')

  // 获取配置
  const obsOptions = ctx.getConfig(CONSTANTS.CONFIG_PATH)

  try {
    // 验证配置
    validateConfig(obsOptions)

    const images = ctx.output
    logger.info(`待上传图片数量: ${images.length}`)

    // 逐个处理图片
    for (const img of images) {
      try {
        await handleSingleImage(ctx, obsOptions, img)
      } catch (error) {
        // 单张图片上传失败不影响其他图片
        logger.error(`处理图片时出错: ${error.message}`)
        ctx.emit('notification', {
          title: '图片上传失败',
          body: handleError(error)
        })
      }
    }

    logger.info('所有图片处理完成')
    return ctx
  } catch (err) {
    // 处理整体上传过程中的错误
    logger.error(`上传过程发生错误: ${err.message}`)
    logger.error('错误详情:', err)

    ctx.emit('notification', {
      title: '上传失败',
      body: handleError(err)
    })
    throw err
  }
}

/**
 * 插件配置项
 * 定义插件的配置界面
 * 
 * @param {Object} ctx - PicGo 上下文
 * @returns {Array} 配置项数组
 */
const config = (ctx) => {
  // 获取用户已有配置或使用默认值
  const userConfig = ctx.getConfig('picBed.huaweicloud-uploader') || {
    accessKeyId: '',
    accessKeySecret: '',
    bucketName: '',
    endpoint: '',
    path: '',
    imageProcess: '',
    customDomain: '',
    cacheControl: ''
  }

  // 返回配置项定义
  return [
    {
      name: 'accessKeyId',
      type: 'input',
      alias: 'AccessKeyId',
      default: userConfig.accessKeyId || '',
      message: '例如YYTIRPODKPQUFBGNKLPE',
      required: true
    },
    {
      name: 'accessKeySecret',
      type: 'password',
      alias: 'AccessKeySecret',
      default: userConfig.accessKeySecret || '',
      message: '例如sYYTIRPODKPQUFBGNKLPE',
      required: true
    },
    {
      name: 'bucketName',
      type: 'input',
      alias: '桶名称',
      default: userConfig.bucketName || '',
      message: '例如nutpi',
      required: true
    },
    {
      name: 'endpoint',
      type: 'input',
      alias: 'EndPoint',
      default: userConfig.endpoint || '',
      message: '例如obs.cn-north-4.myhuaweicloud.com',
      required: true
    },
    {
      name: 'path',
      type: 'input',
      alias: '存储路径',
      message: '在桶中存储的路径，例如img或img/huawei',
      default: userConfig.path || '',
      required: false
    },
    {
      name: 'imageProcess',
      type: 'input',
      alias: '图片处理参数',
      message: '例如?x-image-process=image/resize,w_800',
      default: userConfig.imageProcess || '',
      required: false
    },
    {
      name: 'customDomain',
      type: 'input',
      alias: '自定义域名',
      message: '例如https://nutpi.net',
      default: userConfig.customDomain || '',
      required: false
    },
    {
      name: 'cacheControl',
      type: 'input',
      alias: 'CacheControl配置',
      message: '例如max-age=31536000',
      default: userConfig.cacheControl || '',
      required: false
    }
  ]
}

/**
 * 插件注册函数
 * 向PicGo注册上传器
 * 
 * @param {Object} ctx - PicGo上下文
 * @returns {Object} 插件对象
 */
module.exports = (ctx) => {
  const register = () => {
    ctx.helper.uploader.register(CONSTANTS.PLUGIN_NAME, {
      handle,
      name: CONSTANTS.DISPLAY_NAME,
      config
    })
  }

  return {
    uploader: CONSTANTS.PLUGIN_NAME,
    register
  }
}